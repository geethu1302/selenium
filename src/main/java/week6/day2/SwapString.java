package week6.day2;

public class SwapString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		StringBuffer s1=new StringBuffer("Test");
		StringBuffer s2=new StringBuffer("Leaf");
		
		System.out.println("S1 ->"+s1);
		System.out.println("S2 ->"+s2);
		
		int length1=s1.length();
		int length2=s2.length();
		
		s1.append(s2);
		s2.append(s1, 0, length1);
		
		
		s1.delete(0,length1);
		s2.delete(0,length2);
		
		System.out.println("After Swapping");
		
		System.out.println("S1 -> "+ s1);
		System.out.println("S2 -> "+ s2);
		

	}

}
