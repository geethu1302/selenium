package week2.day1;
import java.util.*;
import java.util.Map.Entry;

public class DuplicateChar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String text;
		Map<Character,Integer> map=new LinkedHashMap<>();
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the text with duplicate characters");
		text=sc.nextLine();
		char[] ch=text.toCharArray();
		for(char c:ch)
		{
			if(map.containsKey(c))
			{
				map.put(c,map.get(c)+1);
			}
			else
				map.put(c, 1);
		}
		System.out.println("Duplicate characters");
		for(Entry<Character,Integer> eMap : map.entrySet())
		{
			if(eMap.getValue()>1)
			{
				System.out.println(eMap.getKey());
			}
		}
		System.out.println("The characters with maximum count are");
		int max=0;
		for(Entry<Character,Integer> eMap1:map.entrySet())
		{
			if(eMap1.getValue()>=max)
			{
				max=eMap1.getValue();
			}
				
		}
		for(Entry<Character,Integer> eMap1:map.entrySet())
		{
			if(eMap1.getValue()==max)
			{
				System.out.println(eMap1.getKey());
			}
			
		}
		
sc.close();
	}

}
