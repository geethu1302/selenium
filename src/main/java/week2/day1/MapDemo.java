package week2.day1;

import java.util.*;
import java.util.Map.Entry;

public class MapDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Map<Integer,String> map = new LinkedHashMap<>();
		Map<Character,Integer> cmap = new LinkedHashMap<>();
		/*map.put(1,"Keerthana");
		map.put(2, "Divya");
		map.put(3, "Vidya");
		System.out.println("The map entries");
		for(Entry<Integer,String>  eachMap : map.entrySet()) {
			System.out.println(eachMap.getKey()+ " -->" + eachMap.getValue());
		}
		*/
		String txt = "Welcome";
		char[] ch= txt.toCharArray();
		
		for(int i=0;i<ch.length;i++) {
			
			int count=1;
			for(int j=i+1;j<ch.length;j++)
			{
				
				if(ch[i]== ch[j])
				{
					
					ch[j]=' ';
					count++;
				}
			}
			cmap.put(txt.charAt(i),count);
			
			
		}
		
		
		for(Entry<Character,Integer>  eachMap : cmap.entrySet()) {
			System.out.println(eachMap.getKey()+ " -->" + eachMap.getValue());
		}
		

	}

}
