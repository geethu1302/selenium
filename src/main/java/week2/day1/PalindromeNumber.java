package week2.day1;
import java.util.*;

public class PalindromeNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String number;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		number=sc.nextLine();
		char[] ch=number.toCharArray();
		int i,j;
		for(i=0,j=ch.length-1;i<ch.length/2;i++,j--)
		{
				if(ch[i]==ch[j])
					continue;
				else
				{
					System.out.println("Not a palindrome");
					
					break;
				}
				
		}
		if(i==ch.length/2)
			System.out.println("Its a palindrome");
		
		sc.close();
		
		
	}

}
