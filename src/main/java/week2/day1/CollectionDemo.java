package week2.day1;
import java.util.*;

public class CollectionDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> empName = new ArrayList<>();
		empName.add("Keerthana");
		empName.add("Divya");
		empName.add("Vidya");
		empName.add("Divya");
		System.out.println("The names list:");
		for(String s:empName)
		{
			System.out.println(s);
		}
		Collections.sort(empName);
		System.out.println("The sorted list");
		for(String s:empName)
		{
			System.out.println(s);
		}
		//Remove Duplicates
		
		Set<String> nameSet = new HashSet<>();
		for(String s:empName)
		{
			nameSet.add(s);
			
		}
		System.out.println("List of names without duplicates");
		for(String s:nameSet)
		{
			System.out.println(s);
		}
		

	}

}
