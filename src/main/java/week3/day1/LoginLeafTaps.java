package week3.day1;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LoginLeafTaps {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		System.setProperty("webdriver.chrome.driver","./Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByPartialLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("TestLeaf");
		driver.findElementById("createLeadForm_firstName").sendKeys("Keerthana");
		driver.findElementById("createLeadForm_lastName").sendKeys("Aravindhan");
		
		WebElement element=driver.findElementById("createLeadForm_dataSourceId");
		WebElement element1=driver.findElementById("createLeadForm_marketingCampaignId");
		WebElement element2=driver.findElementById("createLeadForm_industryEnumId");
		
		Select sel= new Select(element); 
		sel.selectByVisibleText("Employee");
		Select sel1=new Select(element1);
		sel1.selectByValue("CATRQ_CARNDRIVER");
		
		
		Select sel2=new Select(element2);
		
		List<WebElement> le=sel2.getOptions();
		for(WebElement we:le) {
			if(we.getText().startsWith("Ma"))
			{
				sel2.selectByVisibleText(we.getText());
			}
		}
		
		
		
		//driver.findElementByClassName("smallSubmit").click();
		//driver.close();
		

	}

}
