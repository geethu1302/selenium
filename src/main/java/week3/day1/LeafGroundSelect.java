package week3.day1;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LeafGroundSelect {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/Dropdown.html");
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
		WebElement webe = driver.findElementById("dropdown1");
		Select sel=new Select(webe);
		sel.selectByIndex(1);
		
		WebElement webe2 = driver.findElementByXPath("//select[@name='dropdown2']");
		Select sel2=new Select(webe2);
		sel2.selectByVisibleText("Loadrunner");
		
		WebElement webe3 = driver.findElementById("dropdown3");
		Select sel3=new Select(webe3);
		sel3.selectByValue("2");

		WebElement webe4=driver.findElementByXPath("//select[@class='dropdown']");
		Select sel4=new Select(webe4);
		List<WebElement> optionsList = sel4.getOptions();
		System.out.println("The number of options is "+optionsList.size());

		driver.findElementByXPath("//option[text()='You can also use sendKeys to select']/..").sendKeys("Selenium");
		
		//driver.findElementByXPath("(//div[@class='example'])[5]/select").sendKeys("Selenium");
		

		//Select sel5=new Select(webe5);
		driver.findElementByXPath("//option[text()='Select your programs']/..").sendKeys("Appium");

		
		Thread.sleep(500);
		
		
	}

}
