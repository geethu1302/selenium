package week3.day1;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
public class LeafGroundEdit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","./Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/Edit.html");
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
		driver.findElementById("email").sendKeys("keerthana.anbalagan91@gmail.com");
		
		String text = driver.findElementByXPath("//input[starts-with(@value,'Append')]").getText();
		
		driver.findElementByXPath("//input[starts-with(@value,'Append')]").sendKeys("Hello User",Keys.TAB);
		
		String text2 = driver.findElementByXPath("//input[starts-with(@value,'TestLeaf')]").getText();
		System.out.println(text2);
		
		driver.findElementByXPath("(//input[@name='username'])[2]").clear();
		
		boolean status=driver.findElementByXPath("//label[@for='disabled']/following::input").isEnabled();
		if(!status)
			System.out.println("The text field is disabled");
	}

}
