package week3.day1;

import java.util.Scanner;

public class LeapYearCalc {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long year;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the year");
		year=sc.nextLong();
		
		//leap year calc
		
		if((year%4 == 0) & (year%100 !=0) ||(year%400 == 0))
			System.out.println("Leap Year");
		
		else
			System.out.println("Not a leap year");

	}

}
