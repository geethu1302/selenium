package week3.day1;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class LeafGroundChk {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/checkbox.html");
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
		driver.findElementByXPath("(//label[@for='java'])[1]/following::input[1]").click();
		driver.findElementByXPath("((//label[@for='java'])[1]/following::input[3])[1]").click();
		driver.findElementByXPath("((//label[@for='java'])[1]/following::input[5])[1]").click();
		
		boolean selected = driver.findElementByXPath("//label[text()='Confirm Selenium is checked']/following-sibling::input").isSelected();
		if(selected)
			System.out.println("Selenium is selected");
		
		
		driver.findElementByXPath("//label[text()='DeSelect only checked']/following-sibling::input[2]").click();
		for(int i=1;i<=6;i++) {
		String xpath="//label[text()='Select all below checkboxes ']/following-sibling::input["+i+"]";
		driver.findElementByXPath(xpath).click();
		}
		
	}

}
