package week3.day1;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Erail {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./Drivers\\chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("https://erail.in");
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationTo").clear();
		
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);
		
		
		boolean selected=driver.findElementById("chkSelectDateOnly").isSelected();
		if(selected) {
			driver.findElementById("chkSelectDateOnly").click();
		}
		
		//get the entire table
		
		WebElement table=driver.findElementByXPath("//table[@class='DataTable TrainList']");
		//split the rows
		
		List<WebElement> rows=table.findElements(By.tagName("tr"));
		
		//iterate to get columns
		
		for(int i=0;i<rows.size();i++) {
			
			WebElement eachRow = rows.get(i);
			
			List<WebElement> cols=eachRow.findElements(By.tagName("td"));
			
			String trainName=cols.get(1).getText();
			
			System.out.println(trainName);
						
		}
		
/*for(int i=rows.size()-1;i>=0;i--) {
			
			WebElement eachRow = rows.get(i);
			
			List<WebElement> cols=eachRow.findElements(By.tagName("td"));
			
			String trainName=cols.get(1).getText();
			
			System.out.println(trainName);
			
			
			
			
		}*/
		System.out.println();
		System.out.println("After sortin    ");
		System.out.println();
		driver.findElementByPartialLinkText("Train Name").click();
		
		WebElement table1=driver.findElementByXPath("//table[@class='DataTable TrainList']");
		
		
		List<WebElement> list1=table1.findElements(By.tagName("tr"));
		
		for(int i=0;i<list1.size();i++) {
			WebElement eRow=list1.get(i);
			List<WebElement> eCol=eRow.findElements(By.tagName("td"));
			String trainName=eCol.get(1).getText();
			System.out.println(trainName);
			
			
			
		}
		System.out.println();
		
		
		//comparing two list
		boolean compareList = list1.equals(rows);
		System.out.println(compareList);
		
		


		
		
		
		
		
		
		
		
		
		
		
		

	}

}
