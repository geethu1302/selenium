package week3.day1;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class LeafGroundLink {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/Link.html");
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);

		driver.findElementByXPath("//a[text()='Go to Home Page']").click();
		driver.findElementByXPath(" //img[@src='images/link.png']").click();
		
		String ele=driver.findElementByXPath("//a[text()='Find where am supposed to go without clicking me?']").getAttribute("href");
		System.out.println(ele);
		
		String attribute = driver.findElementByXPath("//a[text()='Verify am I broken?']").getAttribute("href");
		if(attribute.contains("error"))
		{
			System.out.println("The url is broken");
		}
		
		driver.findElementByXPath("//label[text()='(Interact with same link name)']/preceding-sibling::a").click();
		driver.findElementByXPath(" //img[@src='images/link.png']").click();
		
		List<WebElement> list1=driver.findElementsByXPath("//div/a");
		System.out.println(list1.size());
		
	
	}

}
