package week4.day1;


import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CommonMethods {

	public static void main(String[] args) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("http:leaftaps.com/opentaps");
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
		File src=driver.getScreenshotAs(OutputType.FILE);
		File des=new File("./snaps/img1.png");
		FileUtils.copyFile(src, des);
		
		
		//alert code
		
		Alert alert1 = driver.switchTo().alert();
		String alertmsg=alert1.getText();
		alert1.accept();
		alert1.dismiss();
		alert1.sendKeys("hello");
		
		//frames
		
		driver.switchTo().frame("iframeresult");
		driver.switchTo().frame("id or name");
		driver.switchTo().frame(driver.findElementById(""));
		driver.switchTo().defaultContent();
		
		driver.switchTo().parentFrame();
		
		
		//table
		WebElement newTable = driver.findElementByXPath("//table[name='table1']");
		List<WebElement> trows = newTable.findElements(By.tagName("tr"));
		
		 /* List<WebElement> tcols = trows.get(3).findElements(By.tagName("td"));
		  System.out.println(tcols.get(3).getText());
		  
		  */
		  
		  
		 
		
		for(int i=0;i<trows.size();i++)
		{
			WebElement frow = trows.get(i);
			List<WebElement> tcols = frow.findElements(By.tagName("td"));
			String text1=tcols.get(0).getText();
		}
		
		//wait
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		Thread.sleep(2000);
		WebElement element=driver.findElementById("");
		WebDriverWait wait1=new WebDriverWait(driver,10);
		wait1.until(ExpectedConditions.elementToBeClickable(element));
		
		
		//locators
		driver.findElementById("").sendKeys("");
		driver.findElementById("").clear();
		driver.findElementByXPath("").click();
		driver.findElementByClassName("").getAttribute("");
		driver.findElementByLinkText("").getText();
		driver.findElementByCssSelector("").getCssValue("");
		WebElement eName = driver.findElementByName("");
	}

}
